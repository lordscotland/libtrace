#include <dlfcn.h>
#include <execinfo.h>
#include <fcntl.h>
#include <mach-o/dyld_images.h>
#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <syslog.h>

#include "trace.h"

#ifdef __LP64__
#define MH_MAGIC_X MH_MAGIC_64
typedef struct mach_header_64 mach_header_x;
#define LC_SEGMENT_X LC_SEGMENT_64
typedef struct segment_command_64 segment_command_x;
typedef struct nlist_64 nlist_x;
#else
#define MH_MAGIC_X MH_MAGIC
typedef struct mach_header mach_header_x;
#define LC_SEGMENT_X LC_SEGMENT
typedef struct segment_command segment_command_x;
typedef struct nlist nlist_x;
#endif

//#include <launch-cache/dyld_cache_format.h>
#define DYLD_SHARED_CACHE_PATH "/System/Library/Caches/com.apple.dyld/dyld_shared_cache_"
struct dyld_cache_header {
  char magic[16];
  uint32_t mappingOffset;
  uint32_t mappingCount;
  uint32_t imagesOffset;
  uint32_t imagesCount;
  uint64_t dyldBaseAddress;
  uint64_t codeSignatureOffset;
  uint64_t codeSignatureSize;
  uint64_t slideInfoOffset;
  uint64_t slideInfoSize;
  uint64_t localSymbolsOffset;
  uint64_t localSymbolsSize;
  uint8_t uuid[16];
};
struct dyld_cache_local_symbols_info {
  uint32_t nlistOffset;
  uint32_t nlistCount;
  uint32_t stringsOffset;
  uint32_t stringsSize;
  uint32_t entriesOffset;
  uint32_t entriesCount;
};
struct dyld_cache_local_symbols_entry {
  uint32_t dylibOffset;
  uint32_t nlistStartIndex;
  uint32_t nlistCount;
};

static struct fsinfo {
  uintptr_t addr;
  const char* selname;
  const char* clsname;
  char prefix;
}* _FindFS(uintptr_t addr,struct fsinfo* list,unsigned int count,bool exact) {
  struct fsinfo* ptr=list;
  unsigned int n=count;
  while(n){
    struct fsinfo* pivot=&ptr[n>>1];
    if(pivot->addr==addr){return pivot;}
    if(pivot->addr<addr){
      ptr=pivot+1;
      n--;
    }
    n>>=1;
  }
  return (!exact && ptr>list)?ptr-1:NULL;
}

void trace_print(const char* label) {
  int count=128,i;
  void** btbuf;
  while((btbuf=malloc(count*sizeof(void*)))){
    int nbt=backtrace(btbuf,count);
    if(nbt<count){count=nbt;break;}
    free(btbuf);
    count*=2;
  }
  if(!btbuf){return;}
  struct addrinfo {
    intptr_t offset;
    uintptr_t saddr;
    const char* fname;
    const char* sname;
    char* lsname;
  }* addrbuf=malloc(count*sizeof(*addrbuf));
  struct imageinfo {
    const void* fbase;
    intptr_t slide;
    const uint8_t* fsdata;
    const struct dyld_cache_local_symbols_entry* entry;
    unsigned int nfs,nsearch;
    struct fsinfo* fslist;
    int* searchbuf;
  }* imagebuf=malloc(count*sizeof(*imagebuf));
  int isearch=0,naddr=0,nimage=0;
  for (i=1;i<count;i++){
    Dl_info info;
    if(dladdr(btbuf[i],&info)){
      const void* fbase=info.dli_fbase;
      struct imageinfo* imageptr=imagebuf;
      struct imageinfo* imageend=imagebuf+nimage;
      while(imageptr<imageend && imageptr->fbase!=fbase){imageptr++;}
      if(imageptr==imageend){
        intptr_t slide=0;
        const segment_command_x* lcle=NULL;
        const struct linkedit_data_command* lcfs=NULL;
        const mach_header_x* mh=fbase;
        if(mh->magic==MH_MAGIC_X){
          const struct load_command* lc=(const void*)((const char*)mh+sizeof(*mh));
          uint32_t ncmds=mh->ncmds;
          for (;ncmds;ncmds--){
            if(lc->cmd==LC_SEGMENT_X){
              const segment_command_x* lcseg=(const void*)lc;
              if(lcseg->fileoff==0 && lcseg->filesize){slide=(uintptr_t)fbase-lcseg->vmaddr;}
              else if(strcmp(lcseg->segname,"__LINKEDIT")==0){lcle=lcseg;}
            }
            else if(lc->cmd==LC_FUNCTION_STARTS){lcfs=(const void*)lc;}
            lc=(const void*)((const char*)lc+lc->cmdsize);
          }
        }
        imageptr->fbase=fbase;
        imageptr->slide=slide;
        imageptr->fsdata=(lcle && lcfs)?(const uint8_t*)
         (lcle->vmaddr+slide+lcfs->dataoff-lcle->fileoff):NULL;
        imageptr->entry=NULL;
        imageptr->fslist=NULL;
        imageptr->searchbuf=NULL;
        nimage++;
      }
      const uintptr_t addr=(uintptr_t)btbuf[i];
      uintptr_t saddr=(uintptr_t)info.dli_saddr?:addr;
      char* lsname=NULL;
      if(!info.dli_sname){
        unsigned int nfs;
        struct fsinfo* fslist=imageptr->fslist;
        const uint8_t* fsdata;
        if(fslist){nfs=imageptr->nfs;}
        else if((fsdata=imageptr->fsdata)){
          imageptr->fsdata=NULL;
          struct _fsnode {
            struct _fsnode* prev;
            uintptr_t addr;
          }* fsnode=NULL;
          uintptr_t fsaddr=(uintptr_t)fbase,offset=0;
          unsigned int bshift=0;
          for (nfs=0;*fsdata;fsdata++){
            offset+=(*fsdata&0x7f)<<bshift;
            if(*fsdata&0x80){bshift+=7;}
            else if(offset){
              struct _fsnode* node=malloc(sizeof(*node));
              node->prev=fsnode;
              node->addr=fsaddr+=offset;
              fsnode=node;
              offset=0;
              bshift=0;
              nfs++;
            }
          }
          fslist=malloc(nfs*sizeof(*fslist));
          struct fsinfo* fsptr=fslist+nfs;
          while(fsnode){
            fsptr--;
            fsptr->addr=fsnode->addr;
            fsptr->prefix=0;
            struct _fsnode* prev=fsnode->prev;
            free(fsnode);
            fsnode=prev;
          }
          unsigned int ncls;
          const char** clsnames=objc_copyClassNamesForImage(info.dli_fname,&ncls);
          const char** clsnameptr=clsnames;
          const char** clsnameend=clsnameptr+ncls;
          for (;clsnameptr<clsnameend;clsnameptr++){
            Class cls=objc_getClass(*clsnameptr);
            if(!cls){continue;}
            char prefix='-';
            while(1){
              unsigned int count;
              Method* methods=class_copyMethodList(cls,&count);
              Method* methodptr=methods;
              Method* methodend=methodptr+count;
              for (;methodptr<methodend;methodptr++){
                struct fsinfo* fsptr=_FindFS((uintptr_t)
                 method_getImplementation(*methodptr),fslist,nfs,true);
                if(!fsptr){continue;}
                fsptr->selname=sel_getName(method_getName(*methodptr));
                fsptr->clsname=*clsnameptr;
                fsptr->prefix=prefix;
              }
              free(methods);
              if(prefix=='+'){break;}
              prefix='+';
              cls=object_getClass((id)cls);
            }
          }
          free(clsnames);
          imageptr->nfs=nfs;
          imageptr->fslist=fslist;
        }
        const struct fsinfo* fsptr;
        if(fslist && (fsptr=_FindFS(saddr,fslist,nfs,false))){
          saddr=fsptr->addr;
          const char prefix=fsptr->prefix;
          if(prefix){asprintf(&lsname,"%c[%s %s]",prefix,fsptr->clsname,fsptr->selname);}
        }
      }
      else if(strcmp(info.dli_sname,"<redacted>")==0){
        int* searchbuf=imageptr->searchbuf;
        unsigned int nsearch=searchbuf?imageptr->nsearch:0;
        if(!searchbuf){imageptr->searchbuf=searchbuf=malloc((count-i)*sizeof(*searchbuf));}
        searchbuf[nsearch++]=naddr;
        imageptr->nsearch=nsearch;
        isearch=1;
      }
      struct addrinfo* addrptr=&addrbuf[naddr++];
      addrptr->offset=addr-saddr;
      addrptr->saddr=saddr-imageptr->slide;
      addrptr->fname=info.dli_fname;
      addrptr->sname=info.dli_sname;
      addrptr->lsname=lsname;
    }
  }
  free(btbuf);
  const struct imageinfo* imageend=imagebuf+nimage;
  const struct dyld_cache_header* dch;
  if(isearch && syscall(294,&dch)==0 && memcmp(dch->magic,"dyld_v1 ",8)==0){
    const char* arch=dch->magic+8;
    while(*arch==' '){arch++;}
    char pathbuf[sizeof(DYLD_SHARED_CACHE_PATH)+sizeof(dch->magic)];
    const size_t prelen=strlen(DYLD_SHARED_CACHE_PATH);
    strcpy(memcpy(pathbuf,DYLD_SHARED_CACHE_PATH,prelen)+prelen,arch);
    const int fd=open(pathbuf,O_RDONLY);
    struct dyld_cache_header rdch;
    read(fd,&rdch,sizeof(rdch));
    const int pagemask=getpagesize()-1;
    const int pageoff=rdch.localSymbolsOffset&pagemask;
    const uintptr_t mapoffset=rdch.localSymbolsOffset&~pagemask;
    const uintptr_t mapsize=((rdch.localSymbolsOffset+rdch.localSymbolsSize+pagemask)&~pagemask)-mapoffset;
    const char* mapptr=mmap(NULL,mapsize,PROT_READ,MAP_PRIVATE,fd,mapoffset);
    if(mapptr!=MAP_FAILED){
      const struct dyld_cache_local_symbols_info* symbols=(const void*)(mapptr+pageoff);
      const struct dyld_cache_local_symbols_entry* entries=(const void*)((const char*)symbols+symbols->entriesOffset);
      const struct dyld_cache_local_symbols_entry* entryend=entries+symbols->entriesCount;
      const nlist_x* nlists=(const void*)((const char*)symbols+symbols->nlistOffset);
      const char* strings=(const char*)symbols+symbols->stringsOffset;
      struct imageinfo* imageptr=imagebuf;
      for (;imageptr<imageend;imageptr++){
        const int* indexptr=imageptr->searchbuf;
        if(!indexptr){continue;}
        const struct dyld_cache_local_symbols_entry* entry=imageptr->entry;
        if(!entry){
          const char* fbase=imageptr->fbase;
          if(!fbase){continue;}
          imageptr->fbase=NULL;
          const uint32_t dylibOffset=(uintptr_t)fbase-(uintptr_t)dch;
          const struct dyld_cache_local_symbols_entry* entryptr=entries;
          for (;entryptr<entryend;entryptr++){
            if(entryptr->dylibOffset==dylibOffset){imageptr->entry=entry=entryptr;break;}
          }
          if(!entry){continue;}
        }
        const int* indexend=indexptr+imageptr->nsearch;
        const nlist_x* nlist0=nlists+entry->nlistStartIndex;
        const nlist_x* nlistend=nlist0+entry->nlistCount;
        for (;indexptr<indexend;indexptr++){
          const uintptr_t saddr=addrbuf[*indexptr].saddr;
          const nlist_x* nlistptr=nlist0;
          for (;nlistptr<nlistend;nlistptr++){
            const uint32_t strx=nlistptr->n_un.n_strx;
            if(strx && nlistptr->n_value==((nlistptr->n_desc&N_ARM_THUMB_DEF)?saddr&~1:saddr)){
              const char* sname=strings+strx;
              const size_t size=strlen(sname)+1;
              addrbuf[*indexptr].lsname=memcpy(malloc(size),sname,size);
              break;
            }
          }
        }
      }
      munmap((void*)mapptr,mapsize);
    }
    close(fd);
  }
  const struct imageinfo* imageptr=imagebuf;
  for (;imageptr<imageend;imageptr++){
    void* fslist=imageptr->fslist;
    if(fslist){free(fslist);}
    void* searchbuf=imageptr->searchbuf;
    if(searchbuf){free(searchbuf);}
  }
  free(imagebuf);
  char** lines=malloc(naddr*sizeof(*lines));
  int* linelengths=malloc(naddr*sizeof(*linelengths));
  size_t totallen=0;
  for (i=0;i<naddr;i++){
    const struct addrinfo* addrptr=&addrbuf[i];
    char* lsname=addrptr->lsname;
    const char* fname=addrptr->fname;
    const char* fn=strrchr(fname,'/');
    if(fn){fname=fn+1;}
    totallen+=(linelengths[i]=asprintf(&lines[i],"%s: 0x%lx (+%ld): %s",
     fname,addrptr->saddr,addrptr->offset,lsname?:addrptr->sname))+1;
    if(lsname){free(lsname);}
  }
  free(addrbuf);
  char* output=malloc(totallen);
  char* optr=output;
  for (i=0;i<naddr;i++){
    int len=linelengths[i];
    optr=memcpy(optr,lines[i],len)+len;
    *optr++=(i==naddr-1)?0:'\n';
    free(lines[i]);
  }
  free(lines);
  free(linelengths);
  syslog(LOG_NOTICE,"[trace] %s\n%s",label,output);
  free(output);
}
