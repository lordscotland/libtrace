ARCHS = arm64

include theos/makefiles/common.mk

LIBRARY_NAME = libtrace
libtrace_FILES = main.c hook.c
libtrace_LDFLAGS = -lobjc -lsubstrate

include $(THEOS_MAKE_PATH)/library.mk

TOOL_NAME = test
test_FILES = test.m
test_LDFLAGS = -L$(THEOS_OBJ_DIR) -ltrace

include $(THEOS_MAKE_PATH)/tool.mk
