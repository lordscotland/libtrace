#include <libkern/OSAtomic.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <substrate.h>
#include <unistd.h>

#include "trace.h"

struct NSBlock {
  void* isa;
  int flags,reserved;
  void(*invoke)(void*,...);
  union {
    struct {
      unsigned long reserved,size;
      void(*copy_helper)(void* dst,void* src);
      void(*dispose_helper)(void* src);
      const char* signature;
    }* with_copy_dispose;
    struct {
      unsigned long reserved,size;
      const char* signature;
    }* simple;
  } descriptor;
};
struct thread_context {
  struct frame {
    struct frame* prev;
    const void* lr;
    TraceOptions options;
    bool logThis;
  }* frame;
  FILE* fh;
  int depth;
  bool paused;
};

static bool _logEnabled=false;
static struct {
  pthread_rwlock_t lock;
  TraceOptions options;
  unsigned int nfilter,nalloc;
  struct filter_entry {
    void* object;
    TraceOptions options;
  }* filterbuf;
} _logControl;
static struct {
  char buf[32];
  unsigned int length;
} _rootPath;
static Class c_NSBlock,c_NSCFBoolean,c_NSCFNumber,c_NSCFString;
static pthread_key_t _contextKey;

#include <CoreFoundation/CoreFoundation.h>
#define IMPORT_FUNCTION(name) __typeof__(name) (*_##name)
static IMPORT_FUNCTION(CFBooleanGetValue);
static IMPORT_FUNCTION(CFNumberGetType);
static IMPORT_FUNCTION(CFNumberGetValue);
static IMPORT_FUNCTION(CFStringGetCharacters);
static IMPORT_FUNCTION(CFStringGetCharactersPtr);
static IMPORT_FUNCTION(CFStringGetCStringPtr);
static IMPORT_FUNCTION(CFStringGetLength);

static struct filter_entry* _FindEntry(void* object,int cmd) {
  struct filter_entry* ptr=_logControl.filterbuf;
  unsigned int nfilter=_logControl.nfilter,n=nfilter;
  char* end=(void*)(ptr+nfilter);
  while(n){
    struct filter_entry* pivot=&ptr[n>>1];
    if(pivot->object==object){
      if(cmd<0){
        ptr=pivot+1;
        memmove(pivot,ptr,end-(char*)ptr);
        _logControl.nfilter=nfilter-1;
        return NULL;
      }
      return pivot;
    }
    if(pivot->object<object){
      ptr=pivot+1;
      n--;
    }
    n>>=1;
  }
  if(cmd){
    size_t mvsize=end-(char*)ptr;
    unsigned int nalloc=_logControl.nalloc;
    if(++nfilter>nalloc){
      struct filter_entry* buf=_logControl.filterbuf;
      struct filter_entry* newbuf=realloc(buf,(nalloc*=2)*sizeof(*buf));
      if(!newbuf){return NULL;}
      _logControl.filterbuf=newbuf;
      _logControl.nalloc=nalloc;
      ptr+=newbuf-buf;
    }
    memmove(ptr+1,ptr,mvsize);
    ptr->object=object;
    _logControl.nfilter=nfilter;
    return ptr;
  }
  return NULL;
}
static void _PrintObject(void* object,Class cls,TraceOptions options,FILE* fh) {
  if(!(options&TRACE_NOCOLOR)){fputs("\e[33m",fh);}
  if(object){
    if(!cls){cls=object_getClass(object);}
    fprintf(fh,"<%s:%p>",class_getName(cls),object);
    while(cls){
      if(cls==c_NSBlock){
        struct NSBlock* block=object;
        if(block->flags&(1<<30)){
          fprintf(fh,"(%s)",(block->flags&(1<<25))?
           block->descriptor.with_copy_dispose->signature:
           block->descriptor.simple->signature);
        }
        break;
      }
      if(options&TRACE_USE_CF){
        if(cls==c_NSCFBoolean){
          fputs(_CFBooleanGetValue(object)?"=TRUE":"=FALSE",fh);
          break;
        }
        else if(cls==c_NSCFNumber){
          fputc('=',fh);
          union {
            int i;
            long l;
            long long ll;
            float f;
            double d;
          } value;
          switch(_CFNumberGetType(object)){
            case kCFNumberSInt8Type:case kCFNumberSInt16Type:
            case kCFNumberCharType:case kCFNumberShortType:
            case kCFNumberIntType:
              if(!_CFNumberGetValue(object,kCFNumberIntType,&value.i)){goto __undefined;}
              fprintf(fh,"%d",value.i);break;
            case kCFNumberSInt32Type:
            case kCFNumberCFIndexType:case kCFNumberNSIntegerType:
            case kCFNumberLongType:
              if(!_CFNumberGetValue(object,kCFNumberLongType,&value.l)){goto __undefined;}
              fprintf(fh,"%ld",value.l);break;
            case kCFNumberSInt64Type:
            case kCFNumberLongLongType:
              if(!_CFNumberGetValue(object,kCFNumberLongLongType,&value.ll)){goto __undefined;}
              fprintf(fh,"%lld",value.ll);break;
            case kCFNumberFloat32Type:
            case kCFNumberFloatType:
              if(!_CFNumberGetValue(object,kCFNumberFloatType,&value.f)){goto __undefined;}
              fprintf(fh,"%.2g",value.f);break;
            case kCFNumberFloat64Type:
            case kCFNumberCGFloatType:
            case kCFNumberDoubleType:
              if(!_CFNumberGetValue(object,kCFNumberDoubleType,&value.d)){goto __undefined;}
              fprintf(fh,"%.6g",value.d);break;
            default:__undefined:
              fputc('?',fh);break;
          }
          break;
        }
        else if(cls==c_NSCFString){
          const char* const escape0="\a\b\e\f\n\r\t\v\\\"";
          const char* const escape1="abefnrtv\\\"";
          fputs("=\"",fh);
          const int buflen=64;
          UniChar ucbuf[buflen];
          const UniChar* ucptr;
          const char* cptr=_CFStringGetCStringPtr(object,kCFStringEncodingASCII);
          if(!cptr){ucptr=_CFStringGetCharactersPtr(object);}
          CFIndex length=_CFStringGetLength(object);
          for (CFIndex i=0,i0=0;i<length;i++){
            UniChar c;
            if(cptr){c=cptr[i];}
            else if(ucptr){c=ucptr[i];}
            else {
              if(i-i0>buflen){i0=i;}
              if(i==i0){
                const CFIndex remain=length-i0;
                _CFStringGetCharacters(object,CFRangeMake(i0,remain<buflen?remain:buflen),ucbuf);
              }
              c=ucbuf[i-i0];
            }
            const char* eptr=strchr(escape0,c);
            if(eptr){fprintf(fh,"\\%c",escape1[eptr-escape0]);}
            else if(c>0xFF){fprintf(fh,"\\u%04X",c);}
            else if(c<0x20 || c>0x7F){fprintf(fh,"\\x%02X",c);}
            else {fputc(c,fh);}
          }
          fputc('"',fh);
          break;
        }
      }
      cls=class_getSuperclass(cls);
    }
  }
  else {fputs("nil",fh);}
  if(!(options&TRACE_NOCOLOR)){fputs("\e[0m",fh);}
}
#ifdef __arm64__
static inline size_t _Align(size_t n,size_t align) {
  return (align>1)?(n+align-1)&-align:n;
}
struct type_info {
  unsigned int size,align;
  unsigned int hsize;
  char htc;
};
#define GET_TYPE_INFO(T,info_) if(info_){\
  info_->size=sizeof(T);info_->align=__alignof(T);}
static const char* _GetTypeInfo(const char* tptr,struct type_info* info_) {
  const char tc=*tptr;
  bool aggr=(tc=='{');
  if(aggr || tc=='('){
    const char tcx=aggr?'}':')';
    unsigned int align=0,size=0;
    while(*++tptr){if(*tptr=='='){tptr++;break;}}
    while(*tptr){
      if(*tptr==tcx){tptr++;break;}
      if(!(tptr=_GetTypeInfo(tptr,info_))){return NULL;}
      if(!info_){continue;}
      const unsigned int s=info_->size,a=info_->align;
      if(aggr){size=_Align(size,a)+s;}
      else if(s>size){size=s;}
      if(a>align){align=a;}
    }
    if(info_){info_->size=_Align(size,info_->align=align);}
    return tptr;
  }
  else if(tc=='['){
    unsigned int count=0;
    while(*++tptr>='0' && *tptr<='9'){
      if(info_){count=10*count+(*tptr-'0');}
    }
    if(!(tptr=_GetTypeInfo(tptr,info_)) || *tptr!=']'){return NULL;}
    if(info_){info_->size*=count;}
    return tptr+1;
  }
  int inc=1;
  switch(tc){
    case '@':inc=0;
      if(*++tptr=='?'){tptr++;}
      GET_TYPE_INFO(id,info_);break;
    case '#':GET_TYPE_INFO(Class,info_);break;
    case ':':GET_TYPE_INFO(SEL,info_);break;
    case 'd':GET_TYPE_INFO(double,info_);break;
    case 'f':GET_TYPE_INFO(float,info_);break;
    case 'B':GET_TYPE_INFO(BOOL,info_);break;
    case 'c':GET_TYPE_INFO(char,info_);break;
    case 'C':GET_TYPE_INFO(unsigned char,info_);break;
    case 's':GET_TYPE_INFO(short,info_);break;
    case 'S':GET_TYPE_INFO(unsigned short,info_);break;
    case 'i':GET_TYPE_INFO(int,info_);break;
    case 'I':GET_TYPE_INFO(unsigned int,info_);break;
    case 'l':GET_TYPE_INFO(long,info_);break;
    case 'L':GET_TYPE_INFO(unsigned long,info_);break;
    case 'q':GET_TYPE_INFO(long long,info_);break;
    case 'Q':GET_TYPE_INFO(unsigned long long,info_);break;
    case '^':inc=0;
      while(*++tptr==tc);
      if(!(tptr=_GetTypeInfo(tptr,NULL))){break;}
    case '*':GET_TYPE_INFO(void*,info_);break;
    case 'v':if(!info_){break;}
    default:return NULL;
  }
  char htc;
  if(info_ && (htc=info_->htc)){
    if(htc==-1){
      info_->htc=tc;
      info_->hsize=info_->size;
    }
    else if(htc!=tc){info_->htc=0;}
  }
  return tptr+inc;
}
static bool _PrintArg(const void** argp,char tc,TraceOptions options,FILE* fh) {
  if(tc=='@'){
    _PrintObject(*(id*)argp,nil,options,fh);
    return true;
  }
  const char* cseq=(options&TRACE_NOCOLOR)?"":"\e[94m";
  switch(tc){
    case '#':fprintf(fh,"%s@class(%s)",cseq,class_getName(*(Class*)argp));break;
    case ':':fprintf(fh,"%s@selector(%s)",cseq,sel_getName(*(SEL*)argp));break;
    case 'd':fprintf(fh,"%s%.6g",cseq,*(double*)argp);break;
    case 'f':fprintf(fh,"%s%.2g",cseq,*(float*)argp);break;
    case 'B':fprintf(fh,"%s%s",cseq,*(BOOL*)argp?"YES":"NO");break;
    case 'c':fprintf(fh,"%s%d",cseq,*(char*)argp);break;
    case 'C':fprintf(fh,"%s%u",cseq,*(unsigned char*)argp);break;
    case 's':fprintf(fh,"%s%hd",cseq,*(short*)argp);break;
    case 'S':fprintf(fh,"%s%hu",cseq,*(unsigned short*)argp);break;
    case 'i':fprintf(fh,"%s%d",cseq,*(int*)argp);break;
    case 'I':fprintf(fh,"%s%u",cseq,*(unsigned int*)argp);break;
    case 'l':fprintf(fh,"%s%ld",cseq,*(long*)argp);break;
    case 'L':fprintf(fh,"%s%lu",cseq,*(unsigned long*)argp);break;
    case 'q':fprintf(fh,"%s%lld",cseq,*(long long*)argp);break;
    case 'Q':fprintf(fh,"%s%llu",cseq,*(unsigned long long*)argp);break;
    case '^':case '*':
      fputs(cseq,fh);
      if(*argp){fprintf(fh,"%p",*argp);}
      else {fputs("NULL",fh);}
      break;
    default:return false;
  }
  if(*cseq){fputs("\e[0m",fh);}
  return true;
}
#endif
static void _PushContext(int ID,const void* lr,const void** sp) {
  struct thread_context* context=pthread_getspecific(_contextKey);
  struct frame* prev;
  FILE* fh;
  int depth;
  bool paused;
  if(context){
    prev=context->frame;
    fh=context->fh;
    depth=context->depth;
    paused=context->paused;
  }
  else {
    pthread_setspecific(_contextKey,context=malloc(sizeof(*context)));
    prev=NULL;
    context->fh=fh=NULL;
    context->depth=depth=-1;
    paused=false;
  }
  struct frame* frame=malloc(sizeof(struct frame));
  context->frame=frame;
  frame->prev=prev;
  frame->lr=lr;
  if(paused){
    frame->logThis=false;
    return;
  }
  context->paused=true;
  unsigned int xn=0;
  if(ID>=3){ID-=3;xn++;}
  id object;
  Class cls;
  if(ID){
    const struct objc_super* super=sp[xn++];
    object=super->receiver;
    cls=super->class;
    if(ID>1){cls=class_getSuperclass(cls);}
  }
  else {
    object=(id)sp[xn++];
    cls=object?object_getClass(object):nil;
  }
  bool logThis=false,logDefault;
  TraceOptions options;
  pthread_rwlock_t* lockptr=&_logControl.lock;
  pthread_rwlock_rdlock(lockptr);
  if(_logControl.nfilter){
    Class qcls=cls;
    struct filter_entry* entry;
    if(object==(void*)qcls){entry=NULL;}
    else if((entry=_FindEntry(object,0))){qcls=nil;}
    while(qcls && !(entry=_FindEntry(qcls,0))){qcls=class_getSuperclass(qcls);}
    if(entry){
      logThis=true;
      options=entry->options;
    }
    else {logDefault=false;}
  }
  else {logDefault=true;}
  if(!logThis){
    logThis=prev?prev->options&TRACE_DEEP:logDefault;
    options=prev?prev->options:_logControl.options;
  }
  pthread_rwlock_unlock(lockptr);
  frame->options=options;
  if((frame->logThis=logThis)){
    if(!fh){
      static volatile int32_t counter=0;
      char buf[64];
      sprintf(memcpy(buf,_rootPath.buf,_rootPath.length)+_rootPath.length,
       "/%d.log",OSAtomicIncrement32(&counter));
      context->fh=fh=fopen(buf,"wb");
    }
    context->depth=++depth;
    while(depth--){fputc(' ',fh);}
    SEL sel=(SEL)sp[xn++];
    Class mcls=object_getClass((id)cls);
    fprintf(fh,"%c[",(cls==mcls || strcmp(class_getName(cls),class_getName(mcls)))?'+':'-');
    _PrintObject(object,cls,options,fh);
    const char* sname=sel_getName(sel);
#ifdef __arm64__
    Method method;
    if(cls && options&TRACE_LOG_ARGUMENTS && (method=class_getInstanceMethod(cls,sel))){
      unsigned int nskip=xn+1,dn=0;
      size_t so=0;
      const char* tptr=method_getTypeEncoding(method);
      const char* sptr;
      while((sptr=strchr(sname,':'))){
        fputc(' ',fh);
        fwrite(sname,1,++sptr-sname,fh);
        sname=sptr;
        if(!tptr) __argumentError:{
          if(options&TRACE_NOCOLOR){fputc('?',fh);}
          else {fputs("\e[91m?\e[0m",fh);}
          continue;
        }
        char tc;
        struct type_info info;
        info.htc=-1;
        do {
          switch(*tptr){
            case 'O':case 'n':case 'o':case 'N':
            case 'r':case 'V':tptr++;continue;
          }
          tc=*tptr;
          if(!(tptr=_GetTypeInfo(tptr,nskip?NULL:&info))){goto __argumentError;}
          while(*tptr=='+' || *tptr=='-'){tptr++;}
          while(*tptr>='0' && *tptr<='9'){tptr++;}
          if(!nskip){break;}
          nskip--;
        } while(1);
        const bool is_fp=(info.htc=='d' || info.htc=='f');
        const bool is_ptr=(info.size>(is_fp?4*info.hsize:16));
        const bool is_gr=(is_ptr || !is_fp);
        const size_t vsize=is_ptr?sizeof(void*):info.size;
        unsigned int nn=(is_gr?xn:dn)+_Align(vsize,__alignof(void*))/sizeof(void*);
        const void** argp;
        if(nn>8){
          nn=8;
          so=_Align(so,is_ptr?__alignof(void*):info.align);
          argp=(const void**)((char*)&sp[16]+so);
          so+=vsize;
        }
        else {argp=&sp[is_gr?xn:8+dn];}
        if(is_gr){xn=nn;}
        else {dn=nn;}
        if(_PrintArg(argp,tc,options,fh)){continue;}
        if(!(options&TRACE_NOCOLOR)){fputs("\e[95m",fh);}
        fputc(tc,fh);
        if(info.size){
          const TraceOptions nocolor=options|TRACE_NOCOLOR;
          const uint8_t* ptr=is_ptr?*argp:(const void*)argp;
          const unsigned int hstep=is_gr?info.htc?info.hsize:0:sizeof(double);
          const uint8_t* end=ptr+(is_gr?info.size:info.size/info.hsize*hstep);
          unsigned int ia=0;
          while(1){
            if(hstep && _PrintArg((const void**)ptr,info.htc,nocolor,fh)){
              if((ptr+=hstep)>=end){break;}
              fputc(',',fh);
            }
            else {
              fprintf(fh,"%02x",*ptr);
              if(++ptr>=end){break;}
              if(++ia==info.align){ia=0;fputc(' ',fh);}
            }
          }
        }
        fputc(tc=='{'?'}':tc=='('?')':']',fh);
        if(!(options&TRACE_NOCOLOR)){fputs("\e[0m",fh);}
      }
    }
#endif
    if(*sname){fprintf(fh," %s",sname);}
    fputs("]\n",fh);
    fflush(fh);
  }
  context->paused=false;
}
static const void* _PopContext() {
  struct thread_context* context=pthread_getspecific(_contextKey);
  struct frame* frame=context->frame;
  context->frame=frame->prev;
  const void* lr=frame->lr;
  if(frame->logThis){context->depth--;}
  free(frame);
  return lr;
}
static void _DestroyContext(struct thread_context* context){
  fclose(context->fh);
  struct frame* frame=context->frame;
  while(frame){
    struct frame* prev=frame->prev;
    free(frame);
    frame=prev;
  }
  free(context);
}

#ifdef __arm64__
#define _ARGS_PUSH_ \
 "STP d6,d7,[sp,#-16]!\nSTP d4,d5,[sp,#-16]!\n"\
 "STP d2,d3,[sp,#-16]!\nSTP d0,d1,[sp,#-16]!\n"\
 "STP x6,x7,[sp,#-16]!\nSTP x4,x5,[sp,#-16]!\n"\
 "STP x2,x3,[sp,#-16]!\nSTP x0,x1,[sp,#-16]!\n"
#define _ARGS_POP_ \
 "LDP x0,x1,[sp],#16\nLDP x2,x3,[sp],#16\n"\
 "LDP x4,x5,[sp],#16\nLDP x6,x7,[sp],#16\n"\
 "LDP d0,d1,[sp],#16\nLDP d2,d3,[sp],#16\n"\
 "LDP d4,d5,[sp],#16\nLDP d6,d7,[sp],#16\n"
#define DEFINE_HOOK(name,ID) static void* F_##name;\
 static __attribute__((naked)) void f_##name(){__asm__("MOV x10,x8");__asm__(\
 "CBNZ %0,1f\nMOV x8,x10\nBR %1\n1:"_ARGS_PUSH_"MOV w0,%2\nMOV x1,lr\nMOV x2,sp\n"\
 "STP x10,%1,[sp,#-16]!\nBL %3\nLDP x8,x10,[sp],#16\n"_ARGS_POP_\
 "BLR x10\n"_ARGS_PUSH_"BL %4\nMOV lr,x0\n"_ARGS_POP_"RET\n"\
 ::"X"(_logEnabled),"r"(F_##name),"i"(ID),"i"(_PushContext),"i"(_PopContext));}
#else
#define _ARGS_PUSH_ "PUSH {%0,%1,%2,%3}\n"
#define _ARGS_POP_ "POP {%0,%1,%2,%3}\n"
#define DEFINE_HOOK(name,ID) static void* F_##name;\
 static void f_##name(long r0,long r1,long r2,long r3){__asm__(\
 "CMP %4,#0\nBNE 1f\nBX %5\n1:"_ARGS_PUSH_"MOV %0,%6\nMOV %1,lr\nMOV %2,sp\n"\
 "PUSH {%5}\nBL %7\nPOP {%5}\n"_ARGS_POP_"BLX %5\n"_ARGS_PUSH_"BL %8\n"\
 "MOV lr,%0\n"_ARGS_POP_::"r"(r0),"r"(r1),"r"(r2),"r"(r3),\
 "X"(_logEnabled),"r"(F_##name),"i"(ID),"i"(_PushContext),"i"(_PopContext));}
DEFINE_HOOK(objc_msgSend_stret,3)
DEFINE_HOOK(objc_msgSendSuper_stret,4)
DEFINE_HOOK(objc_msgSendSuper2_stret,5)
#endif
DEFINE_HOOK(objc_msgSend,0)
DEFINE_HOOK(objc_msgSendSuper,1)
DEFINE_HOOK(objc_msgSendSuper2,2)

static void _Initialize() {
  char* buf=_rootPath.buf;
  _rootPath.length=sprintf(buf,"/var/ea/%d",getpid());
  mkdir(buf,0755);
  c_NSBlock=objc_getClass("NSBlock");
  pthread_rwlock_init(&_logControl.lock,NULL);
  _logControl.filterbuf=malloc((_logControl.nalloc=8)*sizeof(*_logControl.filterbuf));
  _logControl.nfilter=0;
  pthread_key_create(&_contextKey,(void*)_DestroyContext);
  MSImageRef image=MSGetImageByName("/usr/lib/libobjc.A.dylib");
  MSHookFunction(objc_msgSend,f_objc_msgSend,&F_objc_msgSend);
  MSHookFunction(objc_msgSendSuper,f_objc_msgSendSuper,&F_objc_msgSendSuper);
  MSHookFunction(MSFindSymbol(image,"_objc_msgSendSuper2"),
   f_objc_msgSendSuper2,&F_objc_msgSendSuper2);
#ifndef __arm64__
  MSHookFunction(objc_msgSend_stret,f_objc_msgSend_stret,&F_objc_msgSend_stret);
  MSHookFunction(objc_msgSendSuper_stret,f_objc_msgSendSuper_stret,&F_objc_msgSendSuper_stret);
  MSHookFunction(MSFindSymbol(image,"_objc_msgSendSuper2_stret"),
   f_objc_msgSendSuper2_stret,&F_objc_msgSendSuper2_stret);
#endif
}
static void _InitializeCF() {
  void* lib=dlopen("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation",RTLD_LAZY);
  _CFBooleanGetValue=dlsym(lib,"CFBooleanGetValue");
  _CFNumberGetType=dlsym(lib,"CFNumberGetType");
  _CFNumberGetValue=dlsym(lib,"CFNumberGetValue");
  _CFStringGetCharacters=dlsym(lib,"CFStringGetCharacters");
  _CFStringGetCharactersPtr=dlsym(lib,"CFStringGetCharactersPtr");
  _CFStringGetCStringPtr=dlsym(lib,"CFStringGetCStringPtr");
  _CFStringGetLength=dlsym(lib,"CFStringGetLength");
  c_NSCFBoolean=objc_getClass("__NSCFBoolean");
  c_NSCFNumber=objc_getClass("__NSCFNumber");
  c_NSCFString=objc_getClass("__NSCFString");
}

void trace_start(void* object,TraceOptions options) {
  static pthread_once_t once=PTHREAD_ONCE_INIT;
  pthread_once(&once,_Initialize);
  if(options&TRACE_USE_CF){
    static pthread_once_t once=PTHREAD_ONCE_INIT;
    pthread_once(&once,_InitializeCF);
  }
  pthread_rwlock_t* lockptr=&_logControl.lock;
  pthread_rwlock_wrlock(lockptr);
  if(object){
    struct filter_entry* entry=_FindEntry(object,1);
    entry->options=options;
  }
  else {
    _logControl.nfilter=0;
    _logControl.options=options;
  }
  _logEnabled=true;
  pthread_rwlock_unlock(lockptr);
}
void trace_stop(void* object) {
  if(!_logEnabled){return;}
  pthread_rwlock_t* lockptr=&_logControl.lock;
  pthread_rwlock_wrlock(lockptr);
  if(object){
    _FindEntry(object,-1);
    if(!_logControl.nfilter){_logEnabled=false;}
  }
  else {
    _logControl.nfilter=0;
    _logEnabled=false;
  }
  pthread_rwlock_unlock(lockptr);
}
