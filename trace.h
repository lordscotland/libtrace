#ifndef _TRACE_HEADER
#define _TRACE_HEADER
#include <objc/runtime.h>
#if __cplusplus
extern "C" {
#endif
typedef enum {
  TRACE_LOG_ARGUMENTS=(1<<0),
  TRACE_DEEP=(1<<1),
  TRACE_USE_CF=(1<<2),
  TRACE_NOCOLOR=(1<<3),
} TraceOptions;

void trace_print(const char* label);
void trace_start(void* object,TraceOptions options);
void trace_stop(void* object);
#if __cplusplus
}
#endif
#endif
