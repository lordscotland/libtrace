(function(exports){
var lib=dlopen("libtrace.dylib",RTLD_LAZY);if(!lib){return;}
exports.print=(typedef void(char*))(dlsym(lib,"trace_print"))
exports.start=(typedef void(id,int))(dlsym(lib,"trace_start"))
exports.stop=(typedef void(id))(dlsym(lib,"trace_stop"))
})(exports);
