#include "trace.h"

struct mixed_short {
  int i;
  union {
    char cv[2];
    short s;
  };
  double d;
};
struct mixed_long {
  struct mixed_short v;
  unsigned long l;
};
struct intv_short {
  short sv[8];
};
struct intv_long {
  struct intv_short v;
  short s;
};
struct hfa_short {
  float fv[4];
};
struct hfa_long {
  struct hfa_short v;
  float f;
};

__attribute__((objc_root_class))
@interface RootClass @end
@implementation RootClass
+(void)mixed:(struct mixed_short)mv0 :(struct mixed_long)mv1
intv:(struct intv_short)iv0 :(struct intv_long)iv1
hfa:(struct hfa_short)hfa0 :(struct hfa_long)hfa1 {
  printf("%s size:%ld\n==> (%d,%hd,%.6f)\n",
   @encode(__typeof__(mv0)),sizeof(mv0),
   mv0.i,mv0.s,mv0.d);
  printf("%s size:%ld\n==> (%d,%hd,%.6f,0x%lx)\n",
   @encode(__typeof__(mv1)),sizeof(mv1),
   mv1.v.i,mv1.v.s,mv1.v.d,mv1.l);
  printf("%s size:%ld\n==> (%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd)\n",
   @encode(__typeof__(iv0)),sizeof(iv0),
   iv0.sv[0],iv0.sv[1],iv0.sv[2],iv0.sv[3],
   iv0.sv[4],iv0.sv[5],iv0.sv[6],iv0.sv[7]);
  printf("%s size:%ld\n==> (%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd,%hd)\n",
   @encode(__typeof__(iv1)),sizeof(iv1),
   iv1.v.sv[0],iv1.v.sv[1],iv1.v.sv[2],iv1.v.sv[3],
   iv1.v.sv[4],iv1.v.sv[5],iv1.v.sv[6],iv1.v.sv[7],iv1.s);
  printf("%s size:%ld\n==> (%.2f,%.2f,%.2f,%.2f)\n",
   @encode(__typeof__(hfa0)),sizeof(hfa0),
   hfa0.fv[0],hfa0.fv[1],hfa0.fv[2],hfa0.fv[3]);
  printf("%s size:%ld\n==> (%.2f,%.2f,%.2f,%.2f,%.2f)\n",
   @encode(__typeof__(hfa1)),sizeof(hfa1),
   hfa1.v.fv[0],hfa1.v.fv[1],hfa1.v.fv[2],hfa1.v.fv[3],hfa1.f);
}
@end

@interface SuperClass : NSObject @end
@implementation SuperClass
+(double)add:(char)x0 :(short)x1 :(int)x2 :(long)x3 :(long long)x4 :(short)x5
subtract:(unsigned char)x6 :(unsigned short)x7 :(unsigned int)x8 :(unsigned long)x9 :(unsigned long long)x10 :(short)x11
add:(double)f0 :(double)f1 :(double)f2 :(double)f3 :(double)f4
subtract:(float)d5 :(float)d6 :(float)d7 :(float)d8 :(float)d9 {
  trace_print("+SuperClass");
  return x0+x1+x2+x3+x4+x5-x6-x7-x8-x9-x10-x11+f0+f1+f2+f3+f4-d5-d6-d7-d8-d9;
}
-(NSString*)string:(NSString*)string number:(NSNumber*)number
rect:(CGRect)rect range:(NSRange)range :(NSRange)range1 :(NSRange)range2 :(NSRange)range3
point:(CGPoint)point rect:(CGRect)rect1 integer:(NSInteger)integer {
  return [NSString stringWithFormat:
   @"string:%@ number:%@ rect:(%.6f,%.6f;%.6f,%.6f) range:(%lu,%lu) point:(%.6f,%.6f) integer:(%ld)",
   string,number,rect.origin.x,rect.origin.y,rect.size.width,rect.size.height,
   (unsigned long)range.location,(unsigned long)range.length,point.x,point.y,(long)integer];
}
@end

@interface Test : SuperClass @end
@implementation Test
+(struct mixed_short)get:(short)s :(double)d {
  return (struct mixed_short){.i=s*2,.s=s,
   .d=[Test add:10 :11 :12 :13 :14 :15 subtract:6 :7 :8 :9 :10 :11
   add:d :1.1 :1.2 :1.3 :1.4 subtract:0.5 :0.6 :0.7 :0.8 :0.9]};
}
-(NSString*)description {
  trace_print("-Test");
  return [super.description stringByAppendingString:@"-:)"];
}
@end

int main(int argc,char** argv,char** envp) {@autoreleasepool {
  int mode=(1<argc)?atoi(argv[1]):0;
  bool neg=(mode<0);
  if(neg){mode=-mode;}
  int arg0=(2<argc)?atoi(argv[2]):0;
  if(mode&1){trace_start(NULL,arg0);}
  [RootClass mixed:[Test get:100 :2.5]
   :(struct mixed_long){.v=[Test get:-100 :-2.5],.l=0xfeedface}
   intv:(struct intv_short){.sv={8,7,6,5,4,3,2,1}}
   :(struct intv_long){.v={.sv={18,17,16,15,14,13,12,11}},.s=10}
   hfa:(struct hfa_short){.fv={1.5,3.0,4.5,6.0}}
   :(struct hfa_long){.v={.fv={-1.5,-3.0,-4.5,-6.0}},-7.5}];
  [[[NSObject alloc] init] release];
  if(mode&1 && neg){trace_stop(NULL);}
  Test* test=[[Test alloc] init];
  if(mode&2){trace_start(test,arg0);}
  NSString* string=[test string:@"String" number:@2 rect:CGRectMake(20,30,90,45)
   range:NSMakeRange(0,100) :NSMakeRange(10,90) :NSMakeRange(20,80) :NSMakeRange(30,70)
   point:CGPointMake(10,15) rect:CGRectMake(25,35,100,50) integer:7];
  NSArray* array=[NSArray arrayWithObjects:test,string,^(int arg){},nil];
  printf("%s %lu\n",array.description.UTF8String,(unsigned long)test.retainCount);
  if(mode&2 && neg){trace_stop(test);}
  [test release];
}}
